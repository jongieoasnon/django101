from django.shortcuts import render, HttpResponse

from annoucements.models import Announcements


# Create your views here.
def functionOne(request):

    instances = Announcements.objects.all()

    context = {
        'data': instances
    }
    return render(request, "home.html", context)

def functionTwo(request):
    if request.method == "POST":
        content = request.POST.get("content")
    
        instances = Announcements.objects.create(
            content = content
        )

    context = {
    }
    return render(request, "dashboard.html", context)