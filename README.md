# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Sample Django Project

### How do I get set up? ###

* // install virtualenv and virtualenvwrapper
* // create a virtualenvironment
* mkvirtualenv project_name
* // go to the virtual env
* workon project_name
* 
* // go to the repository
* // install django
* pip install django
* 
* // run the development server
* python manage.py runserver

### Who do I talk to? ###

* Author: Jongie Oasnon